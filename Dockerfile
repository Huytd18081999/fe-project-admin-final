# Sử dụng hình ảnh node mới nhất
FROM node:latest

# Đặt thư mục làm việc trong container
WORKDIR /app

# Copy package.json và yarn.lock (hoặc package-lock.json nếu sử dụng npm) vào thư mục làm việc
COPY package.json yarn.lock ./

# Cài đặt dependencies sử dụng yarn
RUN yarn install

# Copy toàn bộ mã nguồn vào thư mục làm việc
COPY . .

# Build dự án React
RUN yarn build

# Cài đặt serve global
RUN yarn global add serve

# Khởi chạy ứng dụng React sử dụng serve
CMD ["serve", "-s", "build"]
