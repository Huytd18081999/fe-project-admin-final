// types
import { createSlice } from '@reduxjs/toolkit';
import { fetchUser } from 'apis/authentication.api';
import {getAllUser} from "../../apis/user.api";

// initial state
const initialState = {
  profile: {
    id: null,
    username: '',
    roleType: null
  },
  status: 'idle',
  error: null
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchUser.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchUser.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.profile = action.payload?.data;
      })
      .addCase(fetchUser.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message;
      });
  }
});

export default userSlice.reducer;
