// third-party
import { combineReducers } from 'redux';

// project import
import menu from './menu';
import coursesReducer from './course.slice';
import lessonReducer from './lesson.slice';
import categoryReducer from './category.slice';
import myCourseReducer from './myCourse.slice';
import userReducer from './user.slice';

// ==============================|| COMBINE REDUCERS ||============================== //

const reducers = combineReducers(
    {
        menu,
        coursesReducer,
        lessonReducer,
        categoryReducer,
        userReducer,
        myCourseReducer
    }
    );

export default reducers;
