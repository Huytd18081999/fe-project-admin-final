// types
import { createSlice } from '@reduxjs/toolkit';
import { fetchMyCourses } from 'apis/course.api';

// initial state
const initialState = {
  myCourses: [],
  status: 'idle',
  error: null
};

const coursesSlice = createSlice({
  name: 'courses',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchMyCourses.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchMyCourses.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.myCourses = action.payload?.data;
      })
      .addCase(fetchMyCourses.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message;
      });
  }
});
export default coursesSlice.reducer;

// export const { courses, status, fetchCoursesFailure } = coursesSlice.actions;
