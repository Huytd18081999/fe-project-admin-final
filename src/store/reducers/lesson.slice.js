// types
import { createSlice } from "@reduxjs/toolkit";
import { fetchLessons } from "apis/lesson.api";

// initial state
const initialState = {
  lessons: [],
  status: "idle",
  error: null,
  total: 0,
};

function sortByOrderIndex(contents) {
  return contents.sort((a, b) => a.orderIndex - b.orderIndex);
}

const lessonSlice = createSlice({
  name: "lessons",
  initialState,
  reducers: {
    resetLesson: () => {
      return initialState;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchLessons.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchLessons.fulfilled, (state, action) => {
        state.status = "succeeded";
        const lessons = sortByOrderIndex(action.payload.data);
        state.lessons = lessons;
        state.total = action.payload.total;
      })
      .addCase(fetchLessons.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});
export default lessonSlice.reducer;
export const { resetLesson } = lessonSlice.actions;
