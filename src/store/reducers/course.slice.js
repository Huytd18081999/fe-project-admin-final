// types
import { createSlice } from "@reduxjs/toolkit";
import { fetchCourses } from "apis/course.api";

// initial state
const initialState = {
  courses: [],
  total: 0,
  status: "idle",
  error: null,
};

const coursesSlice = createSlice({
  name: "courses",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchCourses.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchCourses.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.courses = action.payload?.data;
        state.total = action.payload.total;
      })
      .addCase(fetchCourses.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});
export default coursesSlice.reducer;

// export const { courses, status, fetchCoursesFailure } = coursesSlice.actions;
