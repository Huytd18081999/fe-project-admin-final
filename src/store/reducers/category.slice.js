// types
import { createSlice } from '@reduxjs/toolkit';
import { fetchCategories } from 'apis/categories.api';

// initial state
const initialState = {
  categories: [],
  status: 'idle',
  error: null
};

const categorySlice = createSlice({
  name: 'category',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchCategories.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchCategories.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.categories = action.payload?.data;
      })
      .addCase(fetchCategories.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message;
      });
  }
});
export default categorySlice.reducer;
