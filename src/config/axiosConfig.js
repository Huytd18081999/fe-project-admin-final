import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://34.122.182.144',
  timeout: 50000,
  headers: {
    'Content-Type': 'application/json'
  }
});
instance.interceptors.request.use((config) => {
  const token = localStorage.getItem('accessToken');
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response && error.response.status === 401) {
      window.location.href = '/login';
    }
    return Promise.reject(error);
  }
);
export default instance;
