import { lazy } from "react";

// project import
import Loadable from "components/Loadable";
import MainLayout from "layout/MainLayout";
import CourseManage from "pages/course/index";
import LessonManage from "pages/Lessons/index";
import QuestionManage from "pages/question/index";
import UserManage from "../pages/user";
import HistoryManage from "../pages/history";

// render - dashboard
const DashboardDefault = Loadable(lazy(() => import("pages/dashboard")));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
  path: "/",
  element: <MainLayout />,
  children: [
    {
      path: "/",
      element: <DashboardDefault />,
    },
    {
      path: "/history-manage",
      element: <HistoryManage />,
    },
    {
      path: "/users-manage",
      element: <UserManage />,
    },
    {
      path: "/courses-manage",
      element: <CourseManage />,
    },
    {
      path: "/lessons-manage",
      element: <LessonManage />,
    },
    {
      path: "/lessons-manage/questions-manage/:lessonId",
      element: <QuestionManage />,
    },

    // {
    //   path: 'dashboard',
    //   children: [
    //     {
    //       path: 'default',
    //       element: <DashboardDefault />
    //     }
    //   ]
    // },
  ],
};

export default MainRoutes;
