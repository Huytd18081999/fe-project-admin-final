const convertToSlug = (str) => {
  if (!str) {
    return '';
  }
  str = str?.toLowerCase()?.trim(); // Chuyển đổi chuỗi thành chữ thường và loại bỏ khoảng trắng ở đầu và cuối

  // Thay thế các ký tự đặc biệt và dấu cách bằng dấu gạch ngang
  str = str.replace(/[\s_]+/g, '-');

  // Loại bỏ các ký tự không phải chữ cái, số hoặc dấu gạch ngang
  str = str.replace(/[^\w-]+/g, '');

  return str;
};

export default convertToSlug;
