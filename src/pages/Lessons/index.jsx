import React, { useState } from "react";
import LessonTable from "./LessonTable";
import { Grid, Typography, Box, Button } from "@mui/material";
import FilterBar from "./FilterBar";
import { PlusOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { fetchCourses } from "apis/course.api";
import { fetchLessons } from "apis/lesson.api";

export default function LessonManage() {
  const dispatch = useDispatch();
  const [openCreate, setOpenCreate] = useState(false);

  const courses = useSelector((state) => state.coursesReducer.courses);
  const statusCourse = useSelector((state) => state.coursesReducer.status);
  const lessons = useSelector((state) => state.lessonReducer.lessons);
  const statusLesson = useSelector((state) => state.lessonReducer.status);

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(20);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    dispatch(fetchLessons({ page: newPage, size: rowsPerPage }));
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleCloseModalCreate = () => setOpenCreate(false);

  const handleSelectValueCourse = (event, value) => {
    dispatch(
      fetchLessons({
        courseId: value?.id,
      })
    );
  };

  useEffect(() => {
    if (statusCourse === "idle") {
      dispatch(fetchCourses({ page: 0, size: 100 }));
    }
    if (statusLesson === "idle") {
      dispatch(fetchLessons({ page: page, size: rowsPerPage }));
    }
  }, [statusCourse, statusLesson, dispatch]);

  return (
    <>
      <Grid container rowSpacing={4.5} columnSpacing={2.75}>
        <Box sx={{ marginTop: "40px", display: "flex", marginLeft: "20px" }}>
          <Typography variant="h2">Quản lý danh sách bài học</Typography>
        </Box>

        <Box
          sx={{
            width: "100%",
            marginTop: "40px",
            display: "flex",
            marginLeft: "20px",
            justifyContent: "space-between",
          }}
        >
          <FilterBar
            courses={courses}
            // value={selectValueCourse}
            handleChange={handleSelectValueCourse}
            isLoading={statusCourse === "loading" || statusLesson === "loading"}
          />
          <Button
            color="primary"
            aria-label="add"
            // size="small"
            variant="contained"
            sx={{ height: "40px", alignItem: "flex-end" }}
            onClick={() => setOpenCreate(true)}
          >
            <Typography sx={{ marginRight: "8px" }}>Thêm mới</Typography>
            <PlusOutlined />
          </Button>
        </Box>
        <LessonTable
          openCreate={openCreate}
          handleCloseModalCreate={handleCloseModalCreate}
          courses={courses}
          lessons={lessons}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Grid>
    </>
  );
}
