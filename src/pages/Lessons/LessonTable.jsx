import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

import { useState } from "react";
import Fab from "@mui/material/Fab";
import { Box, Button, Typography } from "@mui/material";
import { EditOutlined, DeleteOutlined, EyeOutlined } from "@ant-design/icons";
import ModalCustom from "components/Popup/Modal";
import { LoadingOutlined } from "@ant-design/icons";
import LessonDetail from "./LessonDetail";
import FormCreate from "./FormCreate";
import { deleteLesson, fetchLessons } from "apis/lesson.api";
import { useDispatch, useSelector } from "react-redux";
import FormEdit from "./FormEdit";
import { useSnackbar } from "notistack";
import { useNavigate } from "../../../node_modules/react-router-dom/dist/index";
import {
  Backdrop,
  CircularProgress,
  TableFooter,
  TablePagination,
} from "../../../node_modules/@mui/material/index";

export default function LessonTable({
  openCreate,
  handleCloseModalCreate,
  lessons,
  courses,
  rowsPerPage,
  page,
  handleChangePage,
  handleChangeRowsPerPage,
}) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const [openDetail, setOpenDetail] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [loadingRow, setLoadingRow] = useState(false);
  const totalLesson = useSelector((state) => state.lessonReducer.total);
  const statusLesson = useSelector((state) => state.lessonReducer.status);

  const initialCourse = {
    id: null,
    title: "",
    courseId: null,
    description: "",
    orderIndex: null,
  };

  const [lessonRow, setLessonRow] = useState(initialCourse);

  const handleCloseModalDelete = () => setOpenDelete(false);
  const handleManageQuestion = (id) => {
    navigate(`/lessons-manage/questions-manage/${id}`);
  };

  const handleCloseModalEdit = () => {
    setOpenEdit(false);
    setLessonRow(initialCourse);
  };

  const handleDelete = async (id) => {
    setLoadingRow(true);

    try {
      await deleteLesson(id);
      await dispatch(fetchLessons());
      setOpenDelete(false);
      setLessonRow(initialCourse);
      enqueueSnackbar("Xoá bài học thành công!", { variant: "success" });
    } catch (error) {
      enqueueSnackbar("Xoá bài học không thành công, vui lòng thử lại sau", {
        variant: "error",
      });
    } finally {
      setLoadingRow(false);
    }
  };

  return (
    <>
      <TableContainer sx={{ margin: "10px 0px 0px 20px" }} component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>STT</TableCell>
              <TableCell align="left">Tên bài học</TableCell>
              <TableCell align="left">Tên khoá học</TableCell>
              <TableCell align="left">Mô tả</TableCell>
              <TableCell align="left" width={"280px"}>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {lessons.map((row, index) => (
              <TableRow
                key={row.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {++index + page * rowsPerPage}
                </TableCell>
                <TableCell align="left">{row.title}</TableCell>
                <TableCell align="left">
                  {courses.find((o) => o.id === row.courseId)?.title}
                </TableCell>
                <TableCell align="left">{row.description}</TableCell>

                <TableCell align="left">
                  <Box>
                    <Fab
                      onClick={() => {
                        setLessonRow(row);
                        setOpenDetail(true);
                      }}
                      sx={{
                        marginRight: "10px",
                        height: "35px",
                        width: "35px",
                      }}
                      aria-label="see"
                      size="small"
                    >
                      <EyeOutlined />
                    </Fab>

                    <Fab
                      onClick={() => {
                        setLessonRow(row);
                        setOpenEdit(true);
                      }}
                      sx={{
                        marginRight: "10px",
                        height: "35px",
                        width: "35px",
                      }}
                      color="secondary"
                      aria-label="edit"
                      size="small"
                    >
                      <EditOutlined />
                    </Fab>

                    <Fab
                      onClick={() => {
                        setLessonRow(row);
                        setOpenDelete(true);
                      }}
                      sx={{
                        marginRight: "10px",
                        height: "35px",
                        width: "35px",
                      }}
                      color="error"
                      aria-label="delete"
                      size="small"
                    >
                      <DeleteOutlined />
                    </Fab>

                    <a onClick={() => handleManageQuestion(row.id)} href="">
                      Quản lý câu hỏi
                    </a>
                  </Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TableFooter>
            <TablePagination
              rowsPerPageOptions={[5, 10, 20, { label: "All", value: -1 }]}
              count={totalLesson}
              rowsPerPage={rowsPerPage}
              page={page}
              slotProps={{
                select: {
                  inputProps: {
                    "aria-label": "rows per page",
                  },
                  native: true,
                },
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              // disabled={true}
              labelRowsPerPage={""}
            />
          </TableFooter>
        </Table>

        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={statusLesson === "loading"}
        >
          <CircularProgress color="inherit" />
        </Backdrop>

        <ModalCustom
          title={"Chi tiết bài học"}
          open={openDetail}
          handleClose={() => setOpenDetail(false)}
          children={<LessonDetail courseId={lessonRow.id} />}
        />

        <ModalCustom
          title={"Thêm bài học"}
          open={openCreate}
          handleClose={handleCloseModalCreate}
          children={
            <FormCreate
              handleClose={handleCloseModalCreate}
              refresh={() => dispatch(fetchLessons())}
            />
          }
        />

        <ModalCustom
          title={"Sửa bài học"}
          open={openEdit}
          handleClose={handleCloseModalEdit}
          children={
            <FormEdit
              lessonId={lessonRow.id}
              handleClose={handleCloseModalEdit}
              refresh={() => dispatch(fetchLessons())}
            />
          }
        />

        <ModalCustom
          title={"Xoá bài học"}
          open={openDelete}
          handleClose={handleCloseModalDelete}
          children={
            <>
              <Typography>Bạn có chắc chắn muốn xoá bài học này?</Typography>
              <Box
                sx={{
                  width: "100%",
                  display: "flex",
                  marginTop: "20px",
                  justifyContent: "space-between",
                }}
              >
                <Button
                  color="error"
                  aria-label="delete"
                  // size="small"
                  variant="contained"
                  onClick={() => handleDelete(lessonRow.id)}
                  disabled={loadingRow}
                >
                  Xoá
                  {loadingRow && <LoadingOutlined />}
                </Button>

                <Button
                  color="secondary"
                  aria-label="close"
                  // size="small"
                  variant="contained"
                  onClick={handleCloseModalDelete}
                  disabled={loadingRow}
                >
                  Đóng
                </Button>
              </Box>
            </>
          }
        />
      </TableContainer>
    </>
  );
}
