import React, { useEffect, useState } from "react";
import {
  Box,
  Paper,
  Typography,
  Stack,
  Link,
  CircularProgress,
} from "../../../node_modules/@mui/material/index";
import { useDispatch, useSelector } from "react-redux";

import { fetchLessonDetail, fetchLessons } from "apis/lesson.api";

export default function LessonDetail({ courseId }) {
  const dispatch = useDispatch();
  const initialState = {
    id: null,
    courseId: null,
    title: "",
    description: "",
    videoUrl: null,
    content: null,
    orderIndex: null,
  };
  const [detailLesson, setDetailLesson] = useState({
    data: initialState,
    status: "idle",
  });

  const fetchDataFromAPI = async (id) => {
    try {
      const responseData = await fetchLessonDetail(id);
      setDetailLesson({
        data: responseData,
        status: "success",
      });
    } catch (error) {
      console.error(error);
      setDetailLesson({
        data: null,
        status: "fail",
      });
    }
  };

  useEffect(() => {
    fetchDataFromAPI(courseId);
  }, [courseId]);

  return (
    <Box sx={{ width: "70vw", maxWidth: "1200px" }}>
      <Paper style={{ padding: "0 8%", backgroundColor: "black" }}>
        <Box
          sx={{
            margin: "0 auto",
            position: "relative",
            paddingTop: "56.25%",
          }}
        >
          {detailLesson.status === "success" ? (
            <video
              width="100%"
              controls
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                height: "100%",
              }}
            >
              <source src={detailLesson.data?.videoUrl} type="video/mp4" />
            </video>
          ) : (
            <Box
              style={{
                position: "absolute",
                left: "50%",
                top: "50%",
                transform: "translate(-50%, -50%)",
              }}
            >
              <CircularProgress color="secondary" />
            </Box>
          )}
        </Box>
      </Paper>
      <Stack spacing={1} sx={{ marginTop: "20px" }}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "12px",
          }}
        >
          <Typography variant="h3">Nội dung bài</Typography>
        </Box>

        {detailLesson.status === "success" && (
          <>
            <Typography variant="">{detailLesson.data?.description}</Typography>

            <Typography variant="h6">
              Tải bài học:{" "}
              <Link target="_blank" href={detailLesson.data.content}>
                {/*{detailLesson.data?.content?.length > 100*/}
                {/*  ? `${detailLesson.data?.content?.substring(0, 100)}...`*/}
                {/*  : detailLesson.data?.content}*/}
                Tại đây
              </Link>
            </Typography>
          </>
        )}
      </Stack>
    </Box>
  );
}
