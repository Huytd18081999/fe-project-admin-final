import React from "react";
import {
  Box,
  MenuItem,
  InputLabel,
  Autocomplete,
  TextField,
} from "../../../node_modules/@mui/material/index";

export default function FilterBar({ handleChange, isLoading, courses }) {
  return (
    <Box sx={{ minWidth: "200px" }}>
      <InputLabel htmlFor="name-course">Tên khoá học</InputLabel>

      <Autocomplete
        options={courses}
        selectOnFocus
        autoHighlight
        getOptionLabel={(option) => option.title}
        onChange={handleChange}
        sx={{ width: 400 }}
        loading={isLoading}
        renderOption={(props, option) => (
          <Box component="li" {...props} key={option.id}>
            <MenuItem value={option.id}>{option.title}</MenuItem>
          </Box>
        )}
        renderInput={(params) => <TextField {...params} />}
      />
    </Box>
  );
}
