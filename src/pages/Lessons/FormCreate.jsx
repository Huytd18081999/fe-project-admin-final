import React, { useState } from "react";
import { Input } from "@mui/material";
import { makeStyles } from "@mui/styles";

import {
  CloudUploadOutlined,
  LoadingOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import * as Yup from "yup";
import { Formik } from "formik";
import {
  Button,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  Stack,
  Link,
  Box,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";
import AnimateButton from "components/@extended/AnimateButton";
import { useSelector } from "react-redux";
import { uploadImg } from "apis/qCommon.api";
import { createLesson } from "apis/lesson.api";
import { CloseOutlined, FileOutlined } from "@ant-design/icons";
import { useSnackbar } from "notistack";
import {
  Autocomplete,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  TextField,
} from "../../../node_modules/@mui/material/index";

const useStyles = makeStyles((theme) => ({
  input: {
    display: "none",
  },
  boxContainer: {
    minHeight: "140px",
    border: "1px dashed #000",
    padding: "8px",
    borderRadius: "8px",
  },
}));

export default function FormCreate({ handleClose, refresh }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [selectedVideo, setSelectedVideo] = useState(null);
  const [previewVideo, setPreviewVideo] = useState(null);

  const [selectedFile, setSelectedFile] = useState(null);
  const [previewFile, setPreviewFile] = useState(null);

  const courses = useSelector((state) => state.coursesReducer.courses);

  const handleChangeVideo = (event) => {
    const file = event.target.files[0];
    if (file) {
      setSelectedVideo(file);
      setPreviewVideo(URL.createObjectURL(file));
    }
  };

  const handleChangeFile = (event) => {
    const file = event.target.files[0];
    if (file) {
      setSelectedFile(file);
      setPreviewFile(URL.createObjectURL(file));
    }
  };

  const handleCloseVideo = () => {
    setSelectedVideo(null);
    setPreviewVideo(null);
  };

  const onsubmit = async (value) => {
    try {
      const formDataVideo = new FormData();
      const formDataFile = new FormData();

      formDataVideo.append("file", selectedVideo);
      const urlVideo = await uploadImg(formDataVideo);

      formDataFile.append("file", selectedFile);
      const urlContent = await uploadImg(formDataFile);

      const body = {
        ...value,
        urlVideo,
        urlContent,
      };
      await createLesson(body);
      enqueueSnackbar("Tạo bài học mới thành công!", { variant: "success" });
      await refresh();
    } catch (error) {
      enqueueSnackbar(
        "Tạo bài học mới không thành công, vui lòng thử lại sau",
        {
          variant: "error",
        }
      );
      console.log(error);
    } finally {
      handleClose();
    }
  };

  return (
    <div>
      <Formik
        initialValues={{
          title: "",
          courseId: null,
          description: "",
          urlVideo: "",
          urlContent: "",
        }}
        validationSchema={Yup.object().shape({
          title: Yup.string().max(255).required("Vui lòng nhập tên bài học"),
          description: Yup.string()
            .max(255)
            .required("Vui lòng nhập mô tả bài học"),
          courseId: Yup.number().required("Vui chọn khoá học"),
          urlVideo: Yup.mixed().required("Vui lòng chọn video bài học"),
          urlContent: Yup.mixed().required("Vui chọn tài liệu bài học"),
        })}
        onSubmit={onsubmit}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          touched,
          values,
          setFieldValue,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="name-lesson">
                    Tên bài học{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>
                  <OutlinedInput
                    id="title-lesson"
                    type="text"
                    value={values.title}
                    name="title"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    placeholder="Nhập tên bài học"
                    fullWidth
                    disabled={isSubmitting}
                    error={Boolean(touched.title && errors.title)}
                  />
                  {touched.title && errors.title && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-title-lesson"
                    >
                      {errors.title}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="name-lesson">
                    Video bài học{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>
                  <Input
                    accept="video/*"
                    className={classes.input}
                    id="urlVideo"
                    value={values.urlVideo}
                    disabled={isSubmitting}
                    multiple
                    type="file"
                    onChange={(target, ...option) => {
                      handleChange(target, ...option);
                      handleChangeVideo(target);
                    }}
                  />
                  <label htmlFor="urlVideo">
                    <Box
                      className={classes.boxContainer}
                      component="span"
                      display="flex"
                      justifyContent="center"
                    >
                      {previewVideo ? (
                        <Box
                          sx={{
                            backgroundColor: "black",
                            position: "relative",
                            width: "100%",
                          }}
                        >
                          <Button
                            onClick={handleCloseVideo}
                            disabled={isSubmitting}
                            sx={{
                              color: "white",
                              width: "24px",
                              height: "24px",
                              minWidth: "0px",
                              borderRadius: "100%",
                              border: "1px solid white",
                              position: "absolute",
                              top: 6,
                              right: 6,
                              zIndex: 1000,
                            }}
                          >
                            <CloseOutlined />
                          </Button>
                          <Box
                            sx={{
                              width: "calc(100% - 76px)",
                              paddingTop: "calc(56.25% - 76px)",
                              position: "relative",
                              left: "38px",
                            }}
                          >
                            <video
                              height="100%"
                              style={{
                                position: "absolute",
                                top: "0",
                                left: "0",
                                width: "100%",
                              }}
                              controls
                            >
                              <source src={previewVideo} type="video/mp4" />
                            </video>
                          </Box>
                        </Box>
                      ) : (
                        <Box
                          display="flex"
                          sx={{
                            alignContent: "center",
                            flexDirection: "column",
                            justifyContent: "center",
                          }}
                        >
                          <CloudUploadOutlined />
                          <Typography>Chọn video</Typography>
                        </Box>
                      )}
                    </Box>
                  </label>
                  {touched.urlVideo && errors.urlVideo && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-url-video"
                    >
                      {errors.urlVideo}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="description-lesson">
                    Mô tả bài học{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>
                  <OutlinedInput
                    id="description-lesson"
                    type="text"
                    rows={4}
                    multiline
                    value={values.description}
                    name="description"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    placeholder="Nhập mô tả bài học"
                    fullWidth
                    disabled={isSubmitting}
                    error={Boolean(touched.description && errors.description)}
                  />
                  {touched.description && errors.description && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-description-lesson"
                    >
                      {errors.description}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="age-native-helper">
                    Khoá học{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>

                  <Autocomplete
                    options={courses}
                    getOptionLabel={(option) => option.title}
                    onChange={(event, newValue) => {
                      setFieldValue("courseId", newValue ? newValue.id : null);
                    }}
                    fullWidth
                    disabled={isSubmitting}
                    renderOption={(props, option) => (
                      <Box component="li" {...props} key={option.id}>
                        {option.title}
                      </Box>
                    )}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={Boolean(touched.courseId && errors.courseId)}
                        placeholder="Chọn khoá học"
                      />
                    )}
                  />
                  {touched.courseId && errors.courseId && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-courseId-course"
                    >
                      {errors.courseId}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="name-lesson">
                    Tài liệu bài học{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>

                  <Box>
                    <Button
                      component="label"
                      variant="contained"
                      tabIndex={-1}
                      startIcon={<CloudUploadOutlined />}
                      disabled={isSubmitting}
                    >
                      Chọn tài liệu
                      <Input
                        accept="file/*"
                        className={classes.input}
                        id="urlContent"
                        value={values.urlContent}
                        disabled={isSubmitting}
                        multiple
                        type="file"
                        onChange={(target, ...option) => {
                          handleChange(target, ...option);
                          handleChangeFile(target);
                        }}
                      />
                    </Button>
                  </Box>
                  {previewFile && (
                    <Box
                      sx={{
                        backgroundColor: "#E7E7E7",
                        borderRadius: "4px",
                      }}
                    >
                      <ListItem
                        alignItems="flex-start"
                        secondaryAction={
                          <IconButton
                            aria-label="comment"
                            color="error"
                            onClick={() => setPreviewFile(null)}
                            disabled={isSubmitting}
                          >
                            <DeleteOutlined />
                          </IconButton>
                        }
                      >
                        <ListItemIcon>
                          <FileOutlined />
                        </ListItemIcon>
                        <ListItemText
                          primary={
                            <Link target="_blank" href={previewFile}>
                              {selectedFile.name?.length > 100
                                ? `${selectedFile.name?.substring(0, 100)}...`
                                : selectedFile.name}
                            </Link>
                          }
                        />
                      </ListItem>
                    </Box>
                  )}

                  {touched.urlContent && errors.urlContent && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-url-video"
                    >
                      {errors.urlContent}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12} sx={{ mt: -1 }}></Grid>
              {errors.submit && (
                <Grid item xs={12}>
                  <FormHelperText error>{errors.submit}</FormHelperText>
                </Grid>
              )}

              <Grid item xs={12}>
                <AnimateButton>
                  <Button
                    disableElevation
                    disabled={isSubmitting}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                    color="primary"
                  >
                    Tạo bài học
                    {isSubmitting && <LoadingOutlined />}
                  </Button>
                </AnimateButton>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </div>
  );
}
