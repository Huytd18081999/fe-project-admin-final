import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { useDispatch, useSelector } from "react-redux";
import {
  createCategory,
  editCategory,
  fetchCategories,
  deleteCategory,
} from "apis/categories.api";
import { useEffect, useState } from "react";
import Fab from "@mui/material/Fab";
import { Box, Button, Typography } from "@mui/material";
import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import ModalCustom from "components/Popup/Modal";
import FormCreate from "./FormCreate";
import FormEdit from "./FormEdit";
import { LoadingOutlined } from "@ant-design/icons";
import {
  TableFooter,
  TablePagination,
} from "../../../node_modules/@mui/material/index";

// TablePaginationActions.propTypes = {
//   count: PropTypes.number.isRequired,
//   onPageChange: PropTypes.func.isRequired,
//   page: PropTypes.number.isRequired,
//   rowsPerPage: PropTypes.number.isRequired,
// };

export default function CoursesInDashboard() {
  const [openCreate, setOpenCreate] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [loadingRow, setLoadingRow] = useState(false);

  const [categoryRow, setCategoryRow] = useState({ id: null, name: "" });

  const dispatch = useDispatch();
  const categories = useSelector((state) => state.categoryReducer.categories);
  const status = useSelector((state) => state.categoryReducer.status);

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(50);

  // const emptyRows =
  //   page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleCloseModalCreate = () => setOpenCreate(false);
  const handleCloseModalDelete = () => setOpenDelete(false);

  const handleCloseModalEdit = () => {
    setOpenEdit(false);
    setCategoryRow({ id: null, name: "" });
  };

  const handleCreate = async (values) => {
    try {
      await createCategory(values);
      await dispatch(fetchCategories());
      setOpenCreate(false);
    } catch (err) {}
  };

  const handleEdit = async (values) => {
    try {
      await editCategory(values);
      await dispatch(fetchCategories());
      setOpenEdit(false);
      setCategoryRow({ id: null, name: "" });
    } catch (err) {}
  };

  const handleDelete = async (id) => {
    setLoadingRow(true);

    try {
      await deleteCategory(id);
      await dispatch(fetchCategories());
      setOpenDelete(false);
      setCategoryRow({ id: null, name: "" });
    } catch (error) {
    } finally {
      setLoadingRow(false);
    }
  };

  useEffect(() => {
    if (status === "idle") {
      dispatch(fetchCategories());
    }
  }, [status, dispatch]);

  return (
    <>
      <Box
        sx={{
          width: "100%",
          marginTop: "40px",
          display: "flex",
          marginLeft: "20px",
          justifyContent: "space-between",
        }}
      >
        <Typography variant="h2">Quản lý thể loại</Typography>
        <Button
          color="primary"
          aria-label="add"
          // size="small"
          variant="contained"
          onClick={() => setOpenCreate(true)}
        >
          <Typography sx={{ marginRight: "8px" }}>Thêm mới</Typography>
          <PlusOutlined />
        </Button>
      </Box>

      <TableContainer sx={{ margin: "10px 0px 0px 20px" }} component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>STT</TableCell>
              <TableCell align="left">Thể loại</TableCell>
              <TableCell align="left" width={"200px"}>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {categories.map((row, index) => (
              <TableRow
                key={row.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {++index}
                </TableCell>
                <TableCell align="left">{row.name}</TableCell>
                <TableCell align="left">
                  <Box>
                    <Fab
                      onClick={() => {
                        setCategoryRow(row);
                        setOpenEdit(true);
                      }}
                      sx={{
                        marginRight: "10px",
                        height: "35px",
                        width: "35px",
                      }}
                      color="secondary"
                      aria-label="edit"
                      size="small"
                    >
                      <EditOutlined />
                    </Fab>

                    <Fab
                      onClick={() => {
                        setCategoryRow(row);
                        setOpenDelete(true);
                      }}
                      sx={{
                        marginRight: "10px",
                        height: "35px",
                        width: "35px",
                      }}
                      color="error"
                      aria-label="delete"
                      size="small"
                    >
                      <DeleteOutlined />
                    </Fab>
                  </Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TableFooter>
            <TableRow>
              {/*<TablePagination*/}
              {/*  rowsPerPageOptions={[2, 10, 50, { label: "All", value: -1 }]}*/}
              {/*  colSpan={3}*/}
              {/*  count={categories.length}*/}
              {/*  rowsPerPage={rowsPerPage}*/}
              {/*  page={page}*/}
              {/*  slotProps={{*/}
              {/*    select: {*/}
              {/*      inputProps: {*/}
              {/*        "aria-label": "rows per page",*/}
              {/*      },*/}
              {/*      native: true,*/}
              {/*    },*/}
              {/*  }}*/}
              {/*  onPageChange={handleChangePage}*/}
              {/*  onRowsPerPageChange={handleChangeRowsPerPage}*/}
              {/*  // ActionsComponent={TablePaginationActions}*/}
              {/*/>*/}
            </TableRow>
          </TableFooter>
        </Table>

        <ModalCustom
          title={"Thêm thể loại"}
          open={openCreate}
          handleClose={handleCloseModalCreate}
          children={<FormCreate onsubmit={handleCreate} />}
        />

        <ModalCustom
          title={"Sửa thể loại"}
          open={openEdit}
          handleClose={handleCloseModalEdit}
          children={<FormEdit onsubmit={handleEdit} row={categoryRow} />}
        />

        <ModalCustom
          title={"Xoá thể loại"}
          open={openDelete}
          handleClose={handleCloseModalDelete}
          children={
            <>
              <Typography>Bạn có chắc chắn muốn xoá thể loại này?</Typography>
              <Box
                sx={{
                  width: "100%",
                  display: "flex",
                  marginTop: "20px",
                  justifyContent: "space-between",
                }}
              >
                <Button
                  color="error"
                  aria-label="delete"
                  // size="small"
                  variant="contained"
                  onClick={() => handleDelete(categoryRow.id)}
                  disabled={loadingRow}
                >
                  Xoá
                  {loadingRow && <LoadingOutlined />}
                </Button>

                <Button
                  color="secondary"
                  aria-label="close"
                  // size="small"
                  variant="contained"
                  onClick={handleCloseModalDelete}
                  disabled={loadingRow}
                >
                  Đóng
                </Button>
              </Box>
            </>
          }
        />
      </TableContainer>
    </>
  );
}
