import React from "react";
import * as Yup from "yup";
import { Formik } from "formik";
import {
  Button,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  Stack,
} from "@mui/material";
import { LoadingOutlined } from "@ant-design/icons";
import AnimateButton from "components/@extended/AnimateButton";

export default function FormEdit({ onsubmit, row }) {
  return (
    <div>
      <Formik
        initialValues={{
          name: row.name,
        }}
        validationSchema={Yup.object().shape({
          name: Yup.string().max(255).required("Vui lòng nhập tên thể loại"),
        })}
        onSubmit={(value) => onsubmit({ name: value.name, id: row.id })}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          touched,
          values,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="name-category">Tên thể loại</InputLabel>
                  <OutlinedInput
                    id="name-category"
                    type="text"
                    value={values.name}
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    placeholder="Nhập tên thể loại"
                    fullWidth
                    error={Boolean(touched.name && errors.name)}
                    disabled={isSubmitting}
                  />
                  {touched.name && errors.name && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-name-category"
                    >
                      {errors.name}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12} sx={{ mt: -1 }}></Grid>
              {errors.submit && (
                <Grid item xs={12}>
                  <FormHelperText error>{errors.submit}</FormHelperText>
                </Grid>
              )}
              <Grid item xs={12}>
                <AnimateButton>
                  <Button
                    disableElevation
                    disabled={isSubmitting}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                    color="primary"
                  >
                    Sửa thể loại
                    {isSubmitting && <LoadingOutlined />}
                  </Button>
                </AnimateButton>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </div>
  );
}
