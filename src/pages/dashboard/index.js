// material-ui
import { Grid } from "@mui/material";

import CoursesInDashboard from "./CoursesInDashboard";

// ==============================|| DASHBOARD - DEFAULT ||============================== //

const DashboardDefault = () => {
  return (
    <Grid container rowSpacing={4.5} columnSpacing={2.75}>
      <CoursesInDashboard />
    </Grid>
  );
};

export default DashboardDefault;
