import React, { useEffect, useState } from "react";
import {
  deleteQuestions,
  fetchQuestions,
  submitQuestion,
} from "apis/lesson.api";
import { useParams } from "react-router-dom";
import {
  Box,
  Typography,
  Button,
  CircularProgress,
  Backdrop,
} from "@mui/material";
import { PlusOutlined } from "@ant-design/icons";
import ModalCustom from "components/Popup/Modal";
import FormCreate from "./formCreate";
import Answer from "./Answer";
import FormEdit from "./formEdit";
import { LoadingOutlined } from "@ant-design/icons";
import { useSnackbar } from "notistack";

export default function QuestionManage() {
  const { lessonId } = useParams();
  const [questions, setQuestions] = useState([]);
  const [openCreate, setOpenCreate] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [loadingRow, setLoadingRow] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const [selectQuestionId, setSelectQuestionId] = useState(null);

  const [selectData, setSelectData] = useState([]);

  const [titleQuestions, setTitleQuestions] = useState("");
  const [totalQuestions, setTotalQuestions] = useState(0);
  const [loading, setLoading] = useState(false); // Trạng thái loading

  const handleCloseModalCreate = () => {
    setOpenCreate(false);
  };

  const handleCloseModalEdit = () => {
    setOpenEdit(false);
  };

  const handleCloseModalDelete = () => {
    setOpenDelete(false);
  };

  const handleOpenModalEdit = (data) => {
    setOpenEdit(true);
    setSelectData(data);
  };

  const handleOpenModalDelete = (id) => {
    setOpenDelete(true);
    setSelectQuestionId(id);
  };

  const fetchData = async () => {
    try {
      const data = await fetchQuestions(lessonId);
      setQuestions(
        data?.questions.map((question) => ({ ...question, answered: false }))
      );
      setTitleQuestions(data?.title);
      setTotalQuestions(data?.totalQuestions);
    } catch (error) {
      setQuestions([]);
      setTotalQuestions(0);
      setTitleQuestions("");
      console.error("Error fetching questions:", error);
    }
  };

  const handleDelete = async (id) => {
    setLoadingRow(true);

    try {
      await deleteQuestions(id);
      await fetchData();
      setOpenDelete(false);
      enqueueSnackbar("Xoá câu hỏi thành công!", { variant: "success" });
    } catch (error) {
      enqueueSnackbar("Xoá câu hỏi không thành công, vui lòng thử lại sau", {
        variant: "error",
      });
    } finally {
      setLoadingRow(false);
    }
  };

  useEffect(() => {
    if (lessonId !== undefined) {
      fetchData();
    }
  }, [lessonId]);

  return (
    <Box sx={{ backgroundColor: "white", padding: "16px", minHeight: "100vh" }}>
      <Typography sx={{ marginBottom: "16px" }} variant="h2">
        Quản lý câu hỏi
      </Typography>

      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "40px",
        }}
      >
        <Typography variant="h4">Bài tập bài: {titleQuestions}</Typography>
        <Typography variant="h4">Tổng số câu hỏi: {totalQuestions}</Typography>
      </Box>

      <Box
        sx={{
          display: "flex",
          marginTop: "16px",
          flexDirection: "row-reverse",
          marginBottom: "30px",
        }}
      >
        <Button
          color="primary"
          aria-label="add"
          // size="small"
          variant="contained"
          sx={{ height: "40px", alignItem: "flex-end" }}
          onClick={() => setOpenCreate(true)}
        >
          <Typography sx={{ marginRight: "8px" }}>Thêm mới</Typography>
          <PlusOutlined />
        </Button>
      </Box>

      {questions.map((ques, index) => {
        return (
          <Answer
            key={ques.questionId}
            data={ques}
            index={index}
            handleOpenModalEdit={handleOpenModalEdit}
            handleOpenModalDelete={handleOpenModalDelete}
          />
        );
      })}

      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>

      <ModalCustom
        title={"Thêm câu hỏi"}
        open={openCreate}
        handleClose={handleCloseModalCreate}
        children={
          <FormCreate
            handleClose={handleCloseModalCreate}
            lessonId={lessonId}
            refresh={() => fetchData()}
          />
        }
      />

      <ModalCustom
        title={"Sửa câu hỏi"}
        open={openEdit}
        handleClose={handleCloseModalEdit}
        children={
          <FormEdit
            handleClose={handleCloseModalEdit}
            data={selectData}
            lessonId={lessonId}
            refresh={() => fetchData()}
          />
        }
      />

      <ModalCustom
        title={"Xoá bài học"}
        open={openDelete}
        handleClose={handleCloseModalDelete}
        children={
          <>
            <Typography>Bạn có chắc chắn muốn xoá bài học này?</Typography>
            <Box
              sx={{
                width: "100%",
                display: "flex",
                marginTop: "20px",
                justifyContent: "space-between",
              }}
            >
              <Button
                color="error"
                aria-label="delete"
                // size="small"
                variant="contained"
                onClick={() => handleDelete(selectQuestionId)}
                disabled={loadingRow}
              >
                Xoá
                {loadingRow && <LoadingOutlined />}
              </Button>

              <Button
                color="secondary"
                aria-label="close"
                // size="small"
                variant="contained"
                onClick={handleCloseModalDelete}
                disabled={loadingRow}
              >
                Đóng
              </Button>
            </Box>
          </>
        }
      />
    </Box>
  );
}
