import React, { useState } from "react";

import { LoadingOutlined } from "@ant-design/icons";
import * as Yup from "yup";
import { Formik, FieldArray } from "formik";
import {
  Button,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  Stack,
  Select,
  Typography,
  TextField,
  MenuItem,
} from "@mui/material";
import AnimateButton from "components/@extended/AnimateButton";
import { useSnackbar } from "notistack";
import InputAdornment from "@mui/material/InputAdornment";
import { createQuestions } from "apis/lesson.api";

export default function FormCreate({ handleClose, refresh, lessonId }) {
  const { enqueueSnackbar } = useSnackbar();

  const onsubmit = async (value) => {
    const { question, correctAnswer, answers } = value;
    const body = {
      lessonId: lessonId,
      question,
      correctAnswer,
      choices: answers,
    };
    try {
      await createQuestions(body);
      await refresh();
      enqueueSnackbar("Tạo câu hỏi mới thành công!", { variant: "success" });
      handleClose();
    } catch (error) {
      enqueueSnackbar(
        "Tạo câu hỏi mới không thành công, vui lòng thử lại sau",
        {
          variant: "error",
        }
      );
    }
  };

  return (
    <div>
      <Formik
        initialValues={{
          question: "",
          correctAnswer: "",
          answers: [undefined],
        }}
        validationSchema={Yup.object().shape({
          question: Yup.string().required("Vui lòng nhập câu hỏi"),
          correctAnswer: Yup.string()
            .required("Vui lòng chọn câu trả lời đúng")
            .test(
              "correctAnswer-in-answers",
              "Vui lòng chọn câu trả lời đúng",
              function (value) {
                const { answers } = this.parent;
                return answers.includes(value);
              }
            ),
          answers: Yup.array()
            .of(Yup.string().required("Câu trả lời không được trống"))
            .min(3, "Phải có ít nhất 3 câu trả lời")
            .test(
              "no-empty-strings",
              "Câu trả lời không được rỗng",
              function (value) {
                return value.every(
                  (answer) => answer?.trim() !== "" && answer !== undefined
                );
              }
            )
            .test(
              "not-duplicate",
              "Câu trả lời không được giống nhau",
              function (value) {
                const uniqueAnswers = new Set(value);
                return uniqueAnswers.size === value.length;
              }
            ),
        })}
        onSubmit={onsubmit}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          touched,
          values,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="correctAnswer-lesson">
                    Câu hỏi{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>
                  <OutlinedInput
                    id="correctAnswer-lesson"
                    type="text"
                    value={values.question}
                    name="question"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    placeholder="Nhập câu hỏi"
                    fullWidth
                    disabled={isSubmitting}
                    error={Boolean(touched.question && errors.question)}
                  />
                  {touched.question && errors.question && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-question-lesson"
                    >
                      {errors.question}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel>
                    Câu trả lời{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>

                  <FieldArray name="answers">
                    {({ remove, push }) => (
                      <Stack spacing={1}>
                        {values.answers.length > 0 &&
                          values.answers.map((answer, index) => (
                            <TextField
                              key={index}
                              id={`answers.${index}`}
                              type="text"
                              value={answer}
                              name={`answers.${index}`}
                              onBlur={handleBlur}
                              onChange={handleChange}
                              placeholder={`Nhập câu trả lời`}
                              fullWidth
                              disabled={isSubmitting}
                              InputProps={{
                                endAdornment: (
                                  <InputAdornment position="end">
                                    <Button
                                      color="error"
                                      aria-label="add"
                                      variant="contained"
                                      sx={{
                                        width: "fit-content",
                                      }}
                                      onClick={() => {
                                        remove(index);
                                      }}
                                      disabled={
                                        values.answers.length === 1 ||
                                        isSubmitting
                                      }
                                    >
                                      <Typography>Xoá câu trả lời</Typography>
                                    </Button>
                                  </InputAdornment>
                                ),
                              }}
                            />
                          ))}

                        <Button
                          color="primary"
                          aria-label="add"
                          variant="contained"
                          disabled={isSubmitting}
                          sx={{
                            height: "40px",
                            alignItem: "flex-end",
                            width: "fit-content",
                          }}
                          onClick={() => push()}
                        >
                          <Typography sx={{ marginRight: "8px" }}>
                            Thêm câu trả lời
                          </Typography>
                          {/* <PlusOutlined /> */}
                        </Button>
                      </Stack>
                    )}
                  </FieldArray>
                  {touched.answers && errors.answers && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-answers-lesson"
                    >
                      {errors.answers}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="correctAnswer-lesson">
                    Câu trả lời đúng{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>
                  <Select
                    value={values.correctAnswer}
                    onChange={handleChange}
                    fullWidth
                    inputProps={{
                      name: "correctAnswer",
                      id: "correct-answer",
                    }}
                    placeholder="Nhập câu trả lời đúng"
                    disabled={isSubmitting}
                    error={Boolean(
                      touched.correctAnswer && errors.correctAnswer
                    )}
                  >
                    {values.answers.map((o) => (
                      <MenuItem value={o}>{o}</MenuItem>
                    ))}
                  </Select>

                  {touched.correctAnswer && errors.correctAnswer && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-correctAnswer-lesson"
                    >
                      {errors.correctAnswer}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12} sx={{ mt: -1 }}></Grid>
              {errors.submit && (
                <Grid item xs={12}>
                  <FormHelperText error>{errors.submit}</FormHelperText>
                </Grid>
              )}

              <Grid item xs={12}>
                <AnimateButton>
                  <Button
                    disableElevation
                    disabled={isSubmitting}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                    color="primary"
                  >
                    Tạo câu hỏi mới
                    {isSubmitting && <LoadingOutlined />}
                  </Button>
                </AnimateButton>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </div>
  );
}
