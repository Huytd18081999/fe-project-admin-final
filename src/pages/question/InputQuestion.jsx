import React from "react";
import { Button, Box, Typography, TextField } from "@mui/material";
import { InputAdornment } from "../../../node_modules/@mui/material/index";
export default function InputQuestion({
  answer,
  value,
  handleChange,
  isSubmitting,
  handleDeleteAnswer,
}) {
  const { index, id, content } = answer;
  return (
    <Box sx={{}}>
      <TextField
        id={`correctAnswer-${index}`}
        type="text"
        value={value}
        name={`correctAnswer-${index}`}
        // onBlur={handleBlur}
        onChange={handleChange}
        placeholder={`${index}. Nhập câu trả lời`}
        fullWidth
        disabled={isSubmitting}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Button
                color="primary"
                aria-label="add"
                variant="contained"
                sx={{
                  width: "fit-content",
                }}
                onClick={handleDeleteAnswer}
              >
                <Typography>Xoá câu trả lời</Typography>
              </Button>
            </InputAdornment>
          ),
        }}
      />
    </Box>
  );
}
