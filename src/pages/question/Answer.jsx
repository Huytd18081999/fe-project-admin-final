import React from "react";
import { Box, Typography } from "@mui/material";
import { CheckOutlined } from "@ant-design/icons";
import { Fab, Stack } from "../../../node_modules/@mui/material/index";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";

export default function Answer(props) {
  const { data, index, handleOpenModalEdit, handleOpenModalDelete } = props;
  return (
    <Box mb={3}>
      <Box display="flex" justifyContent="space-between" mb={1}>
        <Typography variant="h5" sx={{ display: "flex", alignItems: "center" }}>
          <Box
            sx={{
              width: "36px",
              height: "36px",
              borderRadius: "100%",
              backgroundColor: "#E9F2FF",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              color: "#3B4E99",
              marginRight: "6px",
            }}
          >
            {index + 1}
          </Box>
          {data.question}?
        </Typography>
        <Box>
          <Fab
            onClick={() => {
              handleOpenModalEdit(data);
            }}
            sx={{
              marginRight: "10px",
              height: "35px",
              width: "35px",
            }}
            color="secondary"
            aria-label="edit"
            size="small"
          >
            <EditOutlined />
          </Fab>

          <Fab
            onClick={() => {
              handleOpenModalDelete(data.questionId);
            }}
            sx={{
              marginRight: "10px",
              height: "35px",
              width: "35px",
            }}
            color="error"
            aria-label="delete"
            size="small"
          >
            <DeleteOutlined />
          </Fab>
        </Box>
      </Box>
      <Stack
        spacing={1}
        sx={{
          marginLeft: "30px",
        }}
      >
        {data?.choices?.map((choice, idx) => (
          <Box
            key={choice}
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              padding: "3px 10px",

              backgroundColor: choice === data.correctAnswer && "#E7F5EA",
            }}
          >
            <Typography>{choice}</Typography>
            {choice === data.correctAnswer && <CheckOutlined color="green" />}
          </Box>
        ))}
      </Stack>
    </Box>
  );
}
