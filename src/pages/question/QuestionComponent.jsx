import React, { useState } from "react";
import {
  Box,
  RadioGroup,
  FormControl,
  FormControlLabel,
  Radio,
  Typography,
} from "@mui/material";

export default function QuestionComponent(props) {
  const {
    data,
    index,
    onAnswered,
    showErrorMessage,
    disabled,
    isCorrect,
    correctAnswer,
  } = props; // Thêm prop showErrorMessage
  const [selectedOption, setSelectedOption] = useState("");
  const [error, setError] = useState(true); // State để kiểm tra lỗi

  console.log("correctAnswer______:", correctAnswer);
  const handleOptionSelect = (event) => {
    const selectedValue = event.target.value;
    setSelectedOption(selectedValue);
    setError(false); // Đặt lại trạng thái lỗi khi người dùng chọn câu trả lời
    // Gửi thông điệp rằng câu hỏi đã được trả lời lên component cha
    onAnswered(data.questionId, selectedValue);
  };

  return (
    <Box sx={{ marginBottom: "16px" }}>
      <Typography variant="h5" sx={{ display: "flex", alignItems: "center" }}>
        <Box
          sx={{
            width: "36px",
            height: "36px",
            borderRadius: "100%",
            backgroundColor: "#E9F2FF",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            color: "#3B4E99",
            marginRight: "6px",
          }}
        >
          {index + 1}
        </Box>
        {data.question}
      </Typography>

      <FormControl
        disabled={disabled}
        component="fieldset"
        sx={{ marginLeft: "50px", width: "calc(100% - 50px)" }}
      >
        <RadioGroup value={selectedOption} onChange={handleOptionSelect}>
          {data?.choices?.map((choice, idx) => {})}
        </RadioGroup>
      </FormControl>

      {showErrorMessage &&
        error && ( // Hiển thị thông báo lỗi nếu cần và có lỗi xảy ra
          <Typography variant="body2" sx={{ color: "red", marginLeft: "50px" }}>
            Vui lòng chọn câu trả lời
          </Typography>
        )}
    </Box>
  );
}
