import {Grid} from "@mui/material";
import HistoryTable from "./HistoryTable";

const HistoryManage = () => {
    return (
        <Grid container rowSpacing={4.5} columnSpacing={2.75}>
            <HistoryTable />
        </Grid>
    );
};

export default HistoryManage;