import React, { useState } from "react";
import { Input } from "@mui/material";
import { makeStyles } from "@mui/styles";
import {
  Autocomplete,
  Box,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "../../../node_modules/@mui/material/index";
import { CloudUploadOutlined, LoadingOutlined } from "@ant-design/icons";
import * as Yup from "yup";
import { Formik } from "formik";
import {
  Button,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  Stack,
} from "@mui/material";
import AnimateButton from "components/@extended/AnimateButton";
import { useSelector } from "react-redux";
import { uploadImg } from "apis/qCommon.api";
import { editCourse } from "apis/course.api";
import { useSnackbar } from "notistack";

const useStyles = makeStyles((theme) => ({
  input: {
    display: "none",
  },
  boxContainer: {
    // width: "320px",
    height: "140px",
    border: "1px dashed #000",
    padding: "8px",
    borderRadius: "8px",
  },
}));

export default function PopupEdit({ handleClose, refresh, row }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const [selectedFile, setSelectedFile] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);
  const categories = useSelector((state) => state.categoryReducer.categories);
  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      setSelectedFile(file);
      setPreviewImage(URL.createObjectURL(file));
    }
  };

  const onsubmit = async (value) => {
    try {
      if (row.thumbnail && !selectedFile) {
        const body = {
          ...value,
          thumbnail: row.thumbnail,
          id: row.id,
        };

        await editCourse(body);
        await refresh();
        return;
      }

      const formData = new FormData();
      formData.append("file", selectedFile);
      const thumbnail = await uploadImg(formData);

      const body = {
        ...value,
        thumbnail: thumbnail,
        id: row.id,
      };

      await editCourse(body);
      enqueueSnackbar("Sửa khoá học thành công!", { variant: "success" });
      await refresh();
    } catch (error) {
      enqueueSnackbar("Sửa khoá học không thành công, vui lòng thử lại sau", {
        variant: "error",
      });
      console.log(error);
    } finally {
      handleClose();
    }
  };

  return (
    <div>
      <Formik
        initialValues={{
          title: row.title,
          categoryId: row.categoryId,
          description: row.description,
          thumbnail: null,
        }}
        validationSchema={Yup.object().shape({
          title: Yup.string().max(255).required("Vui lòng nhập tên thể loại"),
          description: Yup.string()
            .max(255)
            .required("Vui lòng nhập nội dung khoá học"),
          categoryId: Yup.number().required("Vui chọn thể loại khoá học"),
        })}
        onSubmit={onsubmit}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          touched,
          values,
          setFieldValue,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Input
                  accept="image/*"
                  className={classes.input}
                  id="thumbnail"
                  value={values.thumbnail}
                  multiple
                  type="file"
                  onChange={(target, ...option) => {
                    handleChange(target, ...option);
                    handleFileChange(target);
                  }}
                />
                <label htmlFor="thumbnail">
                  <Box
                    className={classes.boxContainer}
                    component="span"
                    display="flex"
                    justifyContent="center"
                  >
                    {selectedFile ? (
                      <img
                        src={previewImage}
                        alt="Preview"
                        style={{ width: "100%", height: "100%" }}
                      />
                    ) : row.thumbnail && !selectedFile ? (
                      <img
                        src={row.thumbnail}
                        alt="Preview"
                        style={{ width: "100%", height: "100%" }}
                      />
                    ) : (
                      <Box
                        display="flex"
                        sx={{
                          alignContent: "center",
                          flexDirection: "column",
                          justifyContent: "center",
                        }}
                      >
                        <CloudUploadOutlined />
                        <Typography>Chọn ảnh</Typography>
                      </Box>
                    )}
                  </Box>
                </label>
                {touched.thumbnail && errors.thumbnail && (
                  <FormHelperText
                    error
                    id="standard-weight-helper-text-thumbnail"
                  >
                    {errors.thumbnail}
                  </FormHelperText>
                )}
              </Grid>
              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="name-course">
                    Tên khoá học{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>
                  <OutlinedInput
                    id="title-course"
                    type="text"
                    value={values.title}
                    name="title"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    placeholder="Nhập tên thể loại"
                    fullWidth
                    disabled={isSubmitting}
                    error={Boolean(touched.title && errors.title)}
                  />
                  {touched.title && errors.title && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-title-course"
                    >
                      {errors.title}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="description-course">
                    Nội dung khoá học{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>
                  <OutlinedInput
                    id="description-course"
                    type="text"
                    rows={4}
                    multiline
                    value={values.description}
                    name="description"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    placeholder="Nhập nội dung khoá học"
                    fullWidth
                    disabled={isSubmitting}
                    error={Boolean(touched.description && errors.description)}
                  />
                  {touched.description && errors.description && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-description-course"
                    >
                      {errors.description}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="age-native-helper">
                    Thể loại khoá học{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </InputLabel>

                  <Autocomplete
                    options={categories}
                    getOptionLabel={(option) => option.name}
                    onChange={(event, newValue) => {
                      setFieldValue(
                        "categoryId",
                        newValue ? newValue.id : null
                      );
                    }}
                    fullWidth
                    value={
                      categories.find(
                        (category) => category.id === values.categoryId
                      ) || null
                    }
                    disabled={isSubmitting}
                    renderOption={(props, option) => (
                      <Box component="li" {...props} key={option.id}>
                        {option.name}
                      </Box>
                    )}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={Boolean(touched.categoryId && errors.categoryId)}
                        placeholder="Chọn khoá học"
                      />
                    )}
                  />
                  {touched.categoryId && errors.categoryId && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-categoryId-course"
                    >
                      {errors.categoryId}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>
              <Grid item xs={12} sx={{ mt: -1 }}></Grid>
              {errors.submit && (
                <Grid item xs={12}>
                  <FormHelperText error>{errors.submit}</FormHelperText>
                </Grid>
              )}
              <Grid item xs={12}>
                <AnimateButton>
                  <Button
                    disableElevation
                    disabled={isSubmitting}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                    color="primary"
                  >
                    Sửa khoá học
                    {isSubmitting && <LoadingOutlined />}
                  </Button>
                </AnimateButton>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </div>
  );
}
