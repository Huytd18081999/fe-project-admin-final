// material-ui
import { Grid } from "@mui/material";
import CourseTable from "./CourseTable";

const CourseManage = () => {
  return (
    <Grid container rowSpacing={4.5} columnSpacing={2.75}>
      <CourseTable />
    </Grid>
  );
};

export default CourseManage;
