import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { useDispatch, useSelector } from "react-redux";
import {
  createCategory,
  editCategory,
  fetchCategories,
} from "apis/categories.api";
import { useEffect, useState } from "react";
import Fab from "@mui/material/Fab";
import { Box, Button, Typography } from "@mui/material";
import {
  PlusOutlined,
  EditOutlined,
  DeleteOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import ModalCustom from "components/Popup/Modal";
import { LoadingOutlined } from "@ant-design/icons";
import { deleteCourse, fetchCourses } from "apis/course.api";
import PopupImg from "./PopupImg";
import PopupPreview from "./PopupPreview";
import PopupCreate from "./PopupCreate";
import PopupEdit from "./PopupEdit";
import { useSnackbar } from "notistack";
import {
  TableFooter,
  TablePagination,
} from "../../../node_modules/@mui/material/index";

export default function CourseTable() {
  const { enqueueSnackbar } = useSnackbar();
  const [openCreate, setOpenCreate] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openImg, setOpenImg] = useState(false);
  const [openPreview, setOpenPreview] = useState(false);

  const [openDelete, setOpenDelete] = useState(false);
  const [loadingRow, setLoadingRow] = useState(false);
  const initialCourse = {
    id: null,
    title: "",
    categoryId: null,
    description: "",
    thumbnail: "",
  };
  const [courseRow, setCourseRow] = useState(initialCourse);

  const dispatch = useDispatch();
  const courses = useSelector((state) => state.coursesReducer.courses);
  const status = useSelector((state) => state.coursesReducer.status);
  const totalCourse = useSelector((state) => state.coursesReducer.total);

  const categories = useSelector((state) => state.categoryReducer.categories);
  const statusCategory = useSelector((state) => state.categoryReducer.status);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(20);

  const handleCloseModalCreate = () => setOpenCreate(false);
  const handleCloseModalDelete = () => setOpenDelete(false);
  const handleClosePopupImg = () => setOpenImg(false);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    dispatch(fetchCourses({ page: newPage, size: rowsPerPage }));
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleCloseModalEdit = () => {
    setOpenEdit(false);
    setCourseRow(initialCourse);
  };

  const handleDelete = async (id) => {
    setLoadingRow(true);

    try {
      await deleteCourse(id);
      await dispatch(fetchCourses({ page: 0, size: 100 }));
      setOpenDelete(false);
      setCourseRow(initialCourse);
      enqueueSnackbar("Xoá khoá học thành công!", { variant: "success" });
    } catch (error) {
      enqueueSnackbar("Xoá khoá học không thành công, vui lòng thử lại sau", {
        variant: "error",
      });
    } finally {
      setLoadingRow(false);
    }
  };

  useEffect(() => {
    if (status === "idle") {
      dispatch(fetchCourses({ page: page, size: rowsPerPage }));
    }
    if (statusCategory === "idle") {
      dispatch(fetchCategories());
    }
  }, [status, dispatch]);

  return (
    <>
      <Box
        sx={{
          width: "100%",
          marginTop: "40px",
          display: "flex",
          marginLeft: "20px",
          justifyContent: "space-between",
        }}
      >
        <Typography variant="h2">Quản lý khoá học</Typography>
        <Button
          color="primary"
          aria-label="add"
          // size="small"
          variant="contained"
          onClick={() => setOpenCreate(true)}
        >
          <Typography sx={{ marginRight: "8px" }}>Thêm mới</Typography>
          <PlusOutlined />
        </Button>
      </Box>

      <TableContainer sx={{ margin: "10px 0px 0px 20px" }} component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>STT</TableCell>
              <TableCell align="left">Tên khoá học</TableCell>
              <TableCell align="left">Thể loại</TableCell>
              <TableCell align="left">Mô tả</TableCell>
              <TableCell align="left">Hình nền</TableCell>
              <TableCell align="left" width={"200px"}>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {courses.map((row, index) => (
              <TableRow
                key={row.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {++index + page * rowsPerPage}
                </TableCell>
                <TableCell align="left">{row.title}</TableCell>
                <TableCell align="left">
                  {categories.find((o) => o.id === row.categoryId)?.name}
                </TableCell>
                <TableCell align="left">{row.description}</TableCell>
                <TableCell align="left">
                  <img
                    style={{
                      maxWidth: "200px",
                      maxHeight: "100px",
                      cursor: "pointer",
                    }}
                    src={row.thumbnail}
                    loading="lazy"
                    onClick={() => setOpenImg(true)}
                  />
                  <PopupImg
                    open={openImg}
                    handleClose={handleClosePopupImg}
                    imageSrc={row.thumbnail}
                  />
                </TableCell>
                <TableCell align="left">
                  <Box>
                    <Fab
                      onClick={() => {
                        setCourseRow(row);
                        setOpenPreview(true);
                      }}
                      sx={{
                        marginRight: "10px",
                        height: "35px",
                        width: "35px",
                      }}
                      aria-label="see"
                      size="small"
                    >
                      <EyeOutlined />
                    </Fab>

                    <Fab
                      onClick={() => {
                        setCourseRow(row);
                        setOpenEdit(true);
                      }}
                      sx={{
                        marginRight: "10px",
                        height: "35px",
                        width: "35px",
                      }}
                      color="secondary"
                      aria-label="edit"
                      size="small"
                    >
                      <EditOutlined />
                    </Fab>

                    <Fab
                      onClick={() => {
                        setCourseRow(row);
                        setOpenDelete(true);
                      }}
                      sx={{
                        marginRight: "10px",
                        height: "35px",
                        width: "35px",
                      }}
                      color="error"
                      aria-label="delete"
                      size="small"
                    >
                      <DeleteOutlined />
                    </Fab>
                  </Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TableFooter>
            <TablePagination
              rowsPerPageOptions={[5, 10, 20, { label: "All", value: -1 }]}
              count={totalCourse}
              rowsPerPage={rowsPerPage}
              page={page}
              slotProps={{
                select: {
                  inputProps: {
                    "aria-label": "rows per page",
                  },
                  native: true,
                },
              }}
              // disabled={true}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </TableFooter>
        </Table>

        <ModalCustom
           open={openCreate}
          handleClose={handleCloseModalCreate}
          children={
            <PopupCreate
              handleClose={handleCloseModalCreate}
              refresh={async () =>
                await dispatch(fetchCourses({ page: 0, size: 100 }))
              }
            />
          }
        />

        <ModalCustom
          title={"Sửa khoá học"}
          open={openEdit}
          handleClose={handleCloseModalEdit}
          children={
            <PopupEdit
              handleClose={handleCloseModalEdit}
              refresh={async () =>
                await dispatch(fetchCourses({ page: 0, size: 100 }))
              }
              row={courseRow}
            />
          }
        />

        <ModalCustom
          title={"Xoá khoá học"}
          open={openDelete}
          handleClose={handleCloseModalDelete}
          children={
            <>
              <Typography>Bạn có chắc chắn muốn xoá khoá học này?</Typography>
              <Box
                sx={{
                  width: "100%",
                  display: "flex",
                  marginTop: "20px",
                  justifyContent: "space-between",
                }}
              >
                <Button
                  color="error"
                  aria-label="delete"
                  // size="small"
                  variant="contained"
                  onClick={() => handleDelete(courseRow.id)}
                  disabled={loadingRow}
                >
                  Xoá
                  {loadingRow && <LoadingOutlined />}
                </Button>

                <Button
                  color="secondary"
                  aria-label="close"
                  // size="small"
                  variant="contained"
                  onClick={handleCloseModalDelete}
                  disabled={loadingRow}
                >
                  Đóng
                </Button>
              </Box>
            </>
          }
        />
      </TableContainer>

      <PopupPreview
        open={openPreview}
        handleClose={() => setOpenPreview(false)}
        title={courseRow.title}
        description={courseRow.description}
        thumbnail={courseRow.thumbnail}
      />
    </>
  );
}
