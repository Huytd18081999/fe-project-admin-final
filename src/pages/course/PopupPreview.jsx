import React from "react";
import { Box, Modal } from "../../../node_modules/@mui/material/index";
import Course from "components/cards/course/index";

export default function PopupPreview({
  open,
  handleClose,
  title,
  description,
  thumbnail,
}) {
  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 400,
            bgcolor: "#FAFAFB",
            border: "2px solid #000",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Course
            title={title}
            description={description}
            thumbnail={thumbnail}
          />
        </Box>
      </Modal>
    </div>
  );
}
