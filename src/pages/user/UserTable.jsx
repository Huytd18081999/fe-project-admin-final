

import * as React from "react";

import {Box, Button, TableFooter, TablePagination, Typography} from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import {useEffect, useState} from "react";
import instance from "config/axiosConfig";
export default function UserTable() {
    const [users, setUsers] = useState([]);
    const [totalUsers, setTotalUsers] = useState(0);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(20);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await instance.get(`user-service/admin-api/user?page=${page}&size=${rowsPerPage}`);
                setUsers(response.data.data);
                setTotalUsers(response.data.total)
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, [page, rowsPerPage]);
    const handleChangePage = async (event, newPage) => {
        setPage(newPage);
        const response = await instance.get(`user-service/admin-api/user?page=${newPage}&size=${rowsPerPage}`);
        setUsers(response.data.data);
        setTotalUsers(response.data.total)
    };

    return (
        <>
            <Box
                sx={{
                    width: "100%",
                    marginTop: "40px",
                    display: "flex",
                    marginLeft: "20px",
                    justifyContent: "space-between",
                }}
            >
                <Typography variant="h2">Quản lý người dùng</Typography>

            </Box>
            <TableContainer sx={{ margin: "10px 0px 0px 20px" }} component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>STT</TableCell>
                            <TableCell align="left">Tên người dùng</TableCell>
                            {/*<TableCell align="left" width={"200px"}>*/}
                            {/*    Action*/}
                            {/*</TableCell>*/}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {users.map((row, index) => (
                            <TableRow
                                key={row.id}
                                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {++index + page * rowsPerPage}
                                </TableCell>
                                <TableCell align="left">{row.username}</TableCell>
                                <TableCell align="left">
                                    <Box>
                                        {/*<Fab*/}
                                        {/*    onClick={() => {*/}
                                        {/*        setCourseRow(row);*/}
                                        {/*        setOpenPreview(true);*/}
                                        {/*    }}*/}
                                        {/*    sx={{*/}
                                        {/*        marginRight: "10px",*/}
                                        {/*        height: "35px",*/}
                                        {/*        width: "35px",*/}
                                        {/*    }}*/}
                                        {/*    aria-label="see"*/}
                                        {/*    size="small"*/}
                                        {/*>*/}
                                        {/*    <EyeOutlined />*/}
                                        {/*</Fab>*/}

                                        {/*<Fab*/}
                                        {/*    onClick={() => {*/}
                                        {/*        setCourseRow(row);*/}
                                        {/*        setOpenEdit(true);*/}
                                        {/*    }}*/}
                                        {/*    sx={{*/}
                                        {/*        marginRight: "10px",*/}
                                        {/*        height: "35px",*/}
                                        {/*        width: "35px",*/}
                                        {/*    }}*/}
                                        {/*    color="secondary"*/}
                                        {/*    aria-label="edit"*/}
                                        {/*    size="small"*/}
                                        {/*>*/}
                                        {/*    <EditOutlined />*/}
                                        {/*</Fab>*/}

                                        {/*<Fab*/}
                                        {/*    onClick={() => {*/}
                                        {/*        setCourseRow(row);*/}
                                        {/*        setOpenDelete(true);*/}
                                        {/*    }}*/}
                                        {/*    sx={{*/}
                                        {/*        marginRight: "10px",*/}
                                        {/*        height: "35px",*/}
                                        {/*        width: "35px",*/}
                                        {/*    }}*/}
                                        {/*    color="error"*/}
                                        {/*    aria-label="delete"*/}
                                        {/*    size="small"*/}
                                        {/*>*/}
                                        {/*    <DeleteOutlined />*/}
                                        {/*</Fab>*/}
                                    </Box>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                    <TableFooter>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 20, { label: "All", value: -1 }]}
                            count={totalUsers}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            slotProps={{
                                select: {
                                    inputProps: {
                                        "aria-label": "rows per page",
                                    },
                                    native: true,
                                },
                            }}
                            // disabled={true}
                            onPageChange={handleChangePage}
                            // onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </TableFooter>
                </Table>
            </TableContainer>
        </>
    )
}