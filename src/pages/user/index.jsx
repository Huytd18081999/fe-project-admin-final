import { Grid } from "@mui/material";
import UserTable from "./UserTable";


const UserManage = () => {
    return (
        <Grid container rowSpacing={4.5} columnSpacing={2.75}>
            <UserTable />
        </Grid>
    );
};

export default UserManage;