// material-ui
import { Box, FormControl, InputAdornment, OutlinedInput } from '@mui/material';

// assets
import { SearchOutlined } from '@ant-design/icons';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
// ==============================|| HEADER CONTENT - SEARCH ||============================== //

const Search = () => {
  const navigate = useNavigate();
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    navigate(`/search/${encodeURIComponent(searchTerm)}`);
  };
  return (
    <Box sx={{ width: '100%', ml: { xs: 0, md: 1 } }}>
      {/*<form onSubmit={handleSearchSubmit}>*/}
      {/*  <FormControl sx={{ width: { xs: '100%', md: 224 } }}>*/}
      {/*    <OutlinedInput*/}
      {/*      size="small"*/}
      {/*      id="header-search"*/}
      {/*      value={searchTerm}*/}
      {/*      onChange={handleSearchChange}*/}
      {/*      startAdornment={*/}
      {/*        <InputAdornment position="start" sx={{ mr: -0.5 }}>*/}
      {/*          <SearchOutlined />*/}
      {/*        </InputAdornment>*/}
      {/*      }*/}
      {/*      aria-describedby="header-search-text"*/}
      {/*      inputProps={{*/}
      {/*        'aria-label': 'weight'*/}
      {/*      }}*/}
      {/*      placeholder="Tìm kiếm"*/}
      {/*    />*/}
      {/*  </FormControl>*/}
      {/*</form>*/}
    </Box>
  );
};

export default Search;
