// assets
import {
  FileTextOutlined,
  HomeFilled,
  BarsOutlined,
  BookOutlined,
  RocketOutlined,
} from "@ant-design/icons";

// icons
const icons = {
  HomeFilled,
  BarsOutlined,
  FileTextOutlined,
  BookOutlined,
  RocketOutlined,
};
// ==============================|| MENU ITEMS - DASHBOARD ||============================== //

const dashboard = {
  id: "group-dashboard",
  // title: 'Navigation',
  type: "group",
  children: [
    {
      id: "categories-manage",
      title: "Quản lý thể loại",
      type: "item",
      url: "/",
      icon: icons.RocketOutlined,
      breadcrumbs: false,
    },
    {
      id: "courses-manage",
      title: "Quản lý khoá học",
      type: "item",
      url: "/courses-manage",
      icon: icons.HomeFilled,
      breadcrumbs: false,
    },
    {
      id: "lessons-manage",
      title: "Quản lý danh sách bài học",
      type: "item",
      url: "/lessons-manage",
      icon: icons.RocketOutlined,
      breadcrumbs: false,
    },
    {
      id: "history-manage",
      title: "Lịch sử bài tập ",
      type: "item",
      url: "/history-manage",
      icon: icons.RocketOutlined,
      breadcrumbs: false,
    },
    {
      id: "users-manage",
      title: "Quản lý người dùng",
      type: "item",
      url: "/users-manage",
      icon: icons.RocketOutlined,
      breadcrumbs: false,
    },

  ],
};

export default dashboard;
