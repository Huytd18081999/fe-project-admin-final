import CourseSkeleton from 'components/Skeleton/CourseSkeleton';
import Course from 'components/cards/course/index';
import React from 'react';
import { Grid } from '@mui/material';

export default function CourseList(props) {
  const { isLoading, courses } = props;

  return (
    <>
      {(isLoading ? Array.from(new Array(12)) : courses).map((item, index) =>
        item ? (
          <Grid item xs={12} sm={6} md={4} lg={3} xl={2} key={item.id}>
            <Course idCourse={item?.id} title={item?.title} description={item?.description} thumbnail={item?.thumbnail} />
          </Grid>
        ) : (
          <Grid item xs={12} sm={6} md={4} lg={3} xl={2} key={index}>
            <CourseSkeleton />
          </Grid>
        )
      )}
    </>
  );
}
