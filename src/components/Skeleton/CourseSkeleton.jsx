import React from 'react';
import { Stack, Box, Skeleton } from '@mui/material';

export default function CourseSkeleton() {
  return (
    <>
      <Box>
        <Stack spacing={1}>
          <Skeleton variant="rectangular" height={140} sx={{ borderRadius: '16px' }} />
          <Skeleton />
          <Skeleton width="60%" />
        </Stack>
      </Box>
    </>
  );
}
