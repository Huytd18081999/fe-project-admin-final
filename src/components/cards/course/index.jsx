import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
// material-ui
import { Box, Stack, Typography } from "@mui/material";

// project import
import { CardMedia } from "../../../../node_modules/@mui/material/index";

// assets

// ==============================|| STATISTICS - ECOMMERCE CARD  ||============================== //

const Course = ({ title, description, thumbnail, idCourse }) => {
  const navigate = useNavigate();

  const handleClick = (id) => {
    // Thay đổi đường dẫn và truyền tham số

    if (idCourse !== undefined) {
      navigate(`/course/${id}`);
    }
  };

  return (
    <Box>
      <Stack spacing={1}>
        <CardMedia
          component="img"
          height="140"
          image={thumbnail}
          sx={{
            borderRadius: "16px",
            cursor: idCourse !== undefined && "pointer",
          }}
          onClick={() => handleClick(idCourse)}
        />
        <Typography
          variant="h5"
          sx={{
            cursor: "pointer",
          }}
        >
          {title}
        </Typography>
        <Typography variant="h8" color="textSecondary">
          {description?.length > 100
            ? `${description.substring(0, 100)}...`
            : description}
        </Typography>
      </Stack>
    </Box>
  );
};

Course.propTypes = {
  title: PropTypes.string || PropTypes.any,
  description: PropTypes.string || PropTypes.any,
  thumbnail: PropTypes.string || PropTypes.any,
  idCourse: PropTypes.string,
};

export default Course;
