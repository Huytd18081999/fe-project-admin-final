import * as React from "react";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import { CloseOutlined } from "@ant-design/icons";
import Typography from "@mui/material/Typography";
import Dialog from "@mui/material/Dialog";

export default function ModalCustom({ title, open, handleClose, children }) {
  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        sx={{
          ".css-druthd-MuiPaper-root-MuiDialog-paper": {
            maxWidth: "1200px",
            minWidth: "70vw",
          },
        }}
      >
        <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          <Typography id="modal-modal-title" variant="h4" component="h2">
            {title}
          </Typography>
        </DialogTitle>
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseOutlined />
        </IconButton>
        <DialogContent dividers>{children}</DialogContent>
      </Dialog>
    </div>
  );
}
