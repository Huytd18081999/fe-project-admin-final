import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Typography } from '@mui/material';

const Popup = ({ open, onClose, success }) => {
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{success ? 'Thành công' : 'Thất bại'}</DialogTitle>
      <DialogContent>
        <Typography variant="body1">
          {success ? 'Bài tập đã được nộp thành công!' : 'Đã có lỗi xảy ra khi nộp bài. Vui lòng thử lại sau.'}
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Đóng</Button>
      </DialogActions>
    </Dialog>
  );
};

export default Popup;
