import instance from "config/axiosConfig";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const fetchLessons = createAsyncThunk(
  "lesson/fetchLesson",
  async (params) => {
    const response = await instance.get(
      "e-learning-service/admin-api/lessons",
      { params }
    );

    return response.data;
  }
);

export const createLesson = async (body) => {
  const response = await instance.post(
    `e-learning-service/admin-api/lessons`,
    body
  );
  return response.data.data;
};

export const editLesson = async (body) => {
  const response = await instance.put(
    `e-learning-service/admin-api/lessons`,
    body
  );
  return response.data.data;
};

export const deleteLesson = async (id) => {
  const response = await instance.delete(
    `e-learning-service/admin-api/lessons/${id}`
  );
  return response.data.data;
};

export const fetchLessonDetail = async (lessonId) => {
  const response = await instance.get(
    `e-learning-service/api/lessons/${lessonId}`
  );
  return response.data.data;
};

export const fetchQuestions = async (lessonId) => {
  const response = await instance.get(
    `e-learning-service/admin-api/questions?lessonId=${lessonId}`
  );
  return response.data.data;
};

export const createQuestions = async (body) => {
  const response = await instance.post(
    `e-learning-service/admin-api/questions`,
    body
  );
  return response.data.data;
};

export const editQuestions = async (body) => {
  const response = await instance.put(
    `e-learning-service/admin-api/questions`,
    body
  );
  return response.data.data;
};

export const deleteQuestions = async (questionId) => {
  const response = await instance.delete(
    `e-learning-service/admin-api/questions/${questionId}`
  );
  return response.data.data;
};

export const submitQuestion = async (body) => {
  const response = await instance.post(
    `e-learning-service/api/questions`,
    body
  );
  return response.data.data;
};

export const fetchTopRank = async (lessonId) => {
  const response = await instance.get(
    `e-learning-service/api/questions/top-rank?lessonId=${lessonId}`
  );
  return response.data.data;
};
