import instance from "config/axiosConfig";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const fetchUsers = createAsyncThunk(
    "courses/fetchUser",
    async (params) => {
        const response = await instance.get(
            "/e-learning-service/public-api/courses",
            { params }
        );
        return response.data;
    }
);

export const getAllUser = async (body) => {
    const response = await instance.put(
        `user-service/admin-api/user`,
        body
    );
    return response.data;
};
