import instance from 'config/axiosConfig';
import { createAsyncThunk } from '@reduxjs/toolkit';
export const loginRequest = async (body) => {
  const response = await instance.post('user-service/oauth/token', body);
  return response.data.data;
};

export const fetchUser = createAsyncThunk('user/fetchUser', async () => {
  const response = await instance.get('user-service/api/user');
  return response.data;
});
