import instance from "config/axiosConfig";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const fetchCategories = createAsyncThunk(
  "categories/fetchCategories",
  async (params) => {
    const response = await instance.get(
      "/e-learning-service/public-api/categories",
      params
    );
    return response.data;
  }
);

export const createCategory = async (body) => {
  const response = await instance.post(
    `/e-learning-service/admin-api/categories`,
    body
  );
  return response.data.data;
};

export const editCategory = async (body) => {
  const response = await instance.put(
    `/e-learning-service/admin-api/categories`,
    body
  );
  return response.data.data;
};

export const deleteCategory = async (id) => {
  const response = await instance.delete(
    `/e-learning-service/admin-api/categories/${id}`
  );
  return response.data.data;
};
