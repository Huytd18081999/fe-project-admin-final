import instance from "config/axiosConfig";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const fetchCourses = createAsyncThunk(
  "courses/fetchCourses",
  async (params) => {
    const response = await instance.get(
      "/e-learning-service/public-api/courses",
      { params }
    );
    return response.data;
  }
);

export const searchCourses = async (params) => {
  const response = await instance.get(
    "/e-learning-service/public-api/courses",
    { params }
  );
  return response.data;
};

export const fetchMyCourses = createAsyncThunk(
  "courses/fetchCoursesOfUser",
  async () => {
    const response = await instance.get(
      "/e-learning-service/api/course/my-course"
    );
    return response.data;
  }
);

export const enrollmentMyCourse = async (courseId) => {
  const response = await instance.get(
    `e-learning-service/api/course/enrollment/${courseId}`
  );
  return response.data;
};

export const deleteCourse = async (courseId) => {
  const response = await instance.delete(
    `e-learning-service/admin-api/course/${courseId}`
  );
  return response.data;
};

export const createCourse = async (body) => {
  const response = await instance.post(
    `e-learning-service/admin-api/course`,
    body
  );
  return response.data;
};

export const editCourse = async (body) => {
  const response = await instance.put(
    `e-learning-service/admin-api/course`,
    body
  );
  return response.data;
};
