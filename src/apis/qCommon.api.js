import instance from "config/axiosConfig";
export const uploadImg = async (body) => {
  const response = await instance.post(
    "e-learning-service/admin-api/files/upload",
    body,
    {
      headers: {
        "Content-Type": "multipart/form-data", // Đặt đúng kiểu dữ liệu của FormData
      },
    }
  );
  return response.data.data;
};
